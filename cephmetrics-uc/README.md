
Link followed to install ceph metrics: [https://github.com/ceph/cephmetrics](https://github.com/ceph/cephmetrics)
```
$ sudo -i
# subscription-manager repos --enable rhel-7-server-optional-rpms --enable rhel-7-server-rhscon-2-installer-rpms
Repository 'rhel-7-server-optional-rpms' is enabled for this system.
Repository 'rhel-7-server-rhscon-2-installer-rpms' is enabled for this system.

# curl -L -o /etc/yum.repos.d/cephmetrics.repo http://download.ceph.com/cephmetrics/rpm-master/el7/cephmetrics.repo
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   468  100   468    0     0    762      0 --:--:-- --:--:-- --:--:--   763

# yum install cephmetrics-ansible 
Loaded plugins: enabled_repos_upload, package_upload, product-id, search-disabled-repos, subscription-manager
cephmetrics                                                                                                                                                             | 2.9 kB  00:00:00     
cephmetrics-noarch                                                                                                                                                      | 2.9 kB  00:00:00     
rhel-7-server-cert-rpms                                                                                                                                                 | 2.4 kB  00:00:00     
rhel-7-server-extras-rpms                                                                                                                                               | 2.0 kB  00:00:00     
rhel-7-server-openstack-13-devtools-rpms                                                                                                                                | 2.3 kB  00:00:00     
rhel-7-server-openstack-13-optools-rpms                                                                                                                                 | 2.3 kB  00:00:00     
rhel-7-server-openstack-13-rpms                                                                                                                                         | 2.3 kB  00:00:00     
rhel-7-server-optional-rpms                                                                                                                                             | 1.8 kB  00:00:00     
rhel-7-server-rh-common-rpms                                                                                                                                            | 2.1 kB  00:00:00     
rhel-7-server-rhceph-3-mon-rpms                                                                                                                                         | 2.3 kB  00:00:00     
rhel-7-server-rhceph-3-osd-rpms                                                                                                                                         | 2.3 kB  00:00:00     
rhel-7-server-rhceph-3-tools-rpms                                                                                                                                       | 2.3 kB  00:00:00     
rhel-7-server-rhscon-2-installer-rpms                                                                                                                                   | 2.3 kB  00:00:00     
rhel-7-server-rpms                                                                                                                                                      | 2.0 kB  00:00:00     
rhel-7-server-satellite-tools-6.2-rpms                                                                                                                                  | 2.1 kB  00:00:00     
rhel-ha-for-rhel-7-server-rpms                                                                                                                                          | 2.0 kB  00:00:00     
(1/2): cephmetrics-noarch/primary_db                                                                                                                                    | 7.3 kB  00:00:00     
(2/2): cephmetrics/x86_64/primary_db                                                                                                                                    |  28 kB  00:00:00     
Resolving Dependencies
--> Running transaction check
---> Package cephmetrics-ansible.x86_64 0:2.0.2-1.el7cp will be installed
--> Finished Dependency Resolution

Dependencies Resolved

===============================================================================================================================================================================================
 Package                                        Arch                              Version                                   Repository                                                    Size
===============================================================================================================================================================================================
Installing:
 cephmetrics-ansible                            x86_64                            2.0.2-1.el7cp                             rhel-7-server-rhceph-3-tools-rpms                            194 k

Transaction Summary
===============================================================================================================================================================================================
Install  1 Package

Total download size: 194 k
Installed size: 2.1 M
Is this ok [y/d/N]: y
Downloading packages:
cephmetrics-ansible-2.0.2-1.el7cp.x86_64.rpm                                                                                                                            | 194 kB  00:00:00     
Running transaction check
Running transaction test
Transaction test succeeded
Running transaction
  Installing : cephmetrics-ansible-2.0.2-1.el7cp.x86_64                                                                                                                                    1/1 
Uploading Package Profile
  Verifying  : cephmetrics-ansible-2.0.2-1.el7cp.x86_64                                                                                                                                    1/1 

Installed:
  cephmetrics-ansible.x86_64 0:2.0.2-1.el7cp                                                                                                                                                   

Complete!
Uploading Enabled Repositories Report
Loaded plugins: product-id, subscription-manager


```
###### NOTE: if name resolution fails for download.ceph.com use public below IP
```
curl -L -o /etc/yum.repos.d/cephmetrics.repo http://158.69.68.124/cephmetrics/rpm-master/el7/cephmetrics.repo
```


1) Place all the hosts with localdomain suffix in /etc/hosts on OSPD: 

[stack@tb7-ospd ansible]$ cat /etc/hosts
127.0.0.1   localhost localhost.localdomain localhost4 localhost4.localdomain4
::1         localhost localhost.localdomain localhost6 localhost6.localdomain6
10.84.118.114 tb7-ospd.localdomain tb7-ospd.mitg-bxb300.cisco.com tb7-ospd
10.84.123.184 rh-satellite rh-satellite.mitg-bxb300.cisco.com
# host entries for ceph-prometheus configuration
# ------------------------------------------------

192.200.0.106  tb7-ultram-osd-compute-0.localdomain   tb7-ultram-osd-compute-0  osd-compute-0
192.200.0.118  tb7-ultram-osd-compute-1.localdomain   tb7-ultram-osd-compute-1  osd-compute-1
192.200.0.109  tb7-ultram-osd-compute-2.localdomain   tb7-ultram-osd-compute-2  osd-compute-2
192.200.0.107  tb7-ultram-osd-compute-3.localdomain   tb7-ultram-osd-compute-3  osd-compute-3
192.200.0.103  tb7-ultram-osd-compute-4.localdomain   tb7-ultram-osd-compute-4  osd-compute-4
192.200.0.125   tb7-ultram-controller-2.localdomain    tb7-ultram-controller-2  controller-2
192.200.0.112   tb7-ultram-controller-1.localdomain    tb7-ultram-controller-1  controller-1
192.200.0.108   tb7-ultram-controller-0.localdomain    tb7-ultram-controller-0  controller-0
192.200.0.127      tb7-ultram-compute-0.localdomain       tb7-ultram-compute-0  compute-0

[stack@tb7-ospd ansible]$ cat add_host_entry_to_all_overcloud_nodes.yml 
- hosts: overcloud
  become: true
  gather_facts: no
  any_errors_fatal: true

  tasks:
  - name: Update /etc/hosts/entry to add osp-director
    lineinfile:
        path:  /etc/hosts
        line:  '192.200.0.1  tb7-ospd.localdomain tb7-ospd tb7-ospd.mitg-bxb300.cisco.com'
        state: present
        backup: yes

[stack@tb7-ospd ansible]$ ansible-playbook -i inventory add_host_entry_to_all_overcloud_nodes.yml

PLAY [overcloud] ******************************************************************************************************************************************************************************

TASK [Update /etc/hosts/entry to add osp-director] ********************************************************************************************************************************************
changed: [192.200.0.106 | tb7-ultram-osd-compute-0]
changed: [192.200.0.109 | tb7-ultram-osd-compute-2]
changed: [192.200.0.127 | tb7-ultram-compute-0]
changed: [192.200.0.125 | tb7-ultram-controller-2]
changed: [192.200.0.112 | tb7-ultram-controller-1]
changed: [192.200.0.118 | tb7-ultram-osd-compute-1]
changed: [192.200.0.103 | tb7-ultram-osd-compute-4]
changed: [192.200.0.107 | tb7-ultram-osd-compute-3]
changed: [192.200.0.108 | tb7-ultram-controller-0]

PLAY RECAP ************************************************************************************************************************************************************************************
192.200.0.103 | tb7-ultram-osd-compute-4 : ok=1 changed=1 unreachable=0  failed=0
192.200.0.106 | tb7-ultram-osd-compute-0 : ok=1 changed=1 unreachable=0  failed=0
192.200.0.107 | tb7-ultram-osd-compute-3 : ok=1 changed=1 unreachable=0  failed=0
192.200.0.108 | tb7-ultram-controller-0 : ok=1  changed=1 unreachable=0  failed=0
192.200.0.109 | tb7-ultram-osd-compute-2 : ok=1 changed=1 unreachable=0  failed=0
192.200.0.112 | tb7-ultram-controller-1 : ok=1  changed=1 unreachable=0  failed=0
192.200.0.118 | tb7-ultram-osd-compute-1 : ok=1 changed=1 unreachable=0  failed=0
192.200.0.125 | tb7-ultram-controller-2 : ok=1  changed=1 unreachable=0  failed=0
192.200.0.127 | tb7-ultram-compute-0 : ok=1     changed=1 unreachable=0  failed=0

3) Change the port of tripleo from 3000 to 3001, grafana uses 3000 as port :

[stack@tb7-ospd ~]$ sudo netstat -tulpn | grep 300
tcp        0      0 192.200.0.1:3000   0.0.0.0:*   LISTEN      4217/httpd
[stack@tb7-ospd ~]$ sudo -i

Change from 3000 to 3001 in the following files:

[root@tb7-ospd ~]# vim /etc/httpd/conf.d/25-tripleo-ui.conf
[root@tb7-ospd ~]# vim /etc/httpd/conf//ports.conf
[root@tb7-ospd ~]# systemctl restart httpd
[root@tb7-ospd ~]# netstat -tulpn | grep 300
tcp        0      0 192.200.0.1:3001        0.0.0.0:*               LISTEN      46206/httpd


4) Create inventory file for ceph-prometheus

[root@tb7-ospd cephmetrics-ansible]# cat inventory.tb7
[ceph-grafana]
tb7-ospd.localdomain  ansible_connection=local

[osds]
tb7-ultram-osd-compute-0.localdomain   ansible_user=heat-admin
tb7-ultram-osd-compute-1.localdomain   ansible_user=heat-admin
tb7-ultram-osd-compute-2.localdomain   ansible_user=heat-admin
tb7-ultram-osd-compute-3.localdomain   ansible_user=heat-admin
tb7-ultram-osd-compute-4.localdomain   ansible_user=heat-admin

[mons]
tb7-ultram-controller-2.localdomain    ansible_user=heat-admin
tb7-ultram-controller-1.localdomain    ansible_user=heat-admin
tb7-ultram-controller-0.localdomain    ansible_user=heat-admin

[mgrs]
tb7-ultram-controller-2.localdomain    ansible_user=heat-admin
tb7-ultram-controller-1.localdomain    ansible_user=heat-admin
tb7-ultram-controller-0.localdomain    ansible_user=heat-admin

5) Docker pull 2 cephmetrics container from laptop or any other which can reach redhat registry

// pull prometheus and dashboard container
# docker pull registry.access.redhat.com/openshift3/prometheus
# docker pull registry.access.redhat.com/rhceph/rhceph-3-dashboard-rhel7

// docker save container 
# docker save $(docker images --format '{{.Repository}}:{{.Tag}}') -o cephmetrics_docker_images.tar

// transfer cephmetrics docker images to Testbed
# scp cephmetrics_docker_images.tar stack@ospd-ip-address:~/
