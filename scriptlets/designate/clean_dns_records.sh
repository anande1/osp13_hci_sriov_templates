#!/usr/local/bin/bash
# This script will delete all DNS (A, AAAA) records that are not
# associated with a port `openstack port list -project <project> 
# -c fixed_ips -f value` This script will delete the load balancer 
# records since the load balancer is not listed as a server
# but it should be ok since the loadbalancer dont use / plan to disable dns
# excuting this will just print the commands that need to be
# excuted only to stdout, then those commands can be stored
# in file and executed 
# please take sample of the recordsets to be deleted and check them
# before performing the delete on actual production clusters

shopt -s expand_aliases

alias openstack="podman run \
--rm docker.com:4443/openstack/client:latest \
/usr/local/bin/openstack"
# --os-cloud cloud1\
# --os-username anande \
# --os-project-name admin

Help()
{
   echo "Clean orphan DNS records"
   echo
   echo "./clean_dns_recordsets -c cloud1 -e cloud2,cloud3"
   echo "options:"
   echo "c     *Required* cluster/cloud to avoid deleting records" 
   echo "e     Zones to exclude from the cleaning"
   echo "h     Print this Help."
}

while getopts ":he:c:" option
do
    case "${option}" in
        h) 
           Help
           exit;;
        e) 
           excluded_zones="${OPTARG}";;
        c) 
           cloud="${OPTARG}";;
    esac
done

if [[ -z $cloud ]]; then
  echo
  Help
  exit 1
fi

main_zone="$cloud.example.com."
echo $main_zone
main_zone_id=$(openstack zone show $main_zone -c id -f value)
if [[ -z $main_zone_id ]]; then
  echo
  echo "Not valid cloud: $cloud zone:$main_zone does not exists"
  echo
  Help
  exit 1
fi

# Zones that should not be cleaned <cloud>.example.com.
# This method should not delete from <cloud>.example.com.
# directly it should delete from local zones which will delete from 
# the zone <cloud>.example.com.

IFS=', ' read -r -a ignore_zones <<< "$excluded_zones"
# ignore_zones+=($main_zone)

originalIFS=$IFS
IFS=$'\n'
echo "*******************************************"
echo "openstack zone list -c name -f value"
echo "*******************************************"
all_zones=$(openstack zone list -c name -f value)

echo "all_zones = $all_zones"
echo "ignore_zones = ${ignore_zones[@]}"
for zone in $all_zones; do
        for ignore_zone in "${ignore_zones[@]}"
        do 
          if [[ "$zone" == "$ignore_zone" ]]; then
              echo " ignoring $zone "
              continue 2
          fi
        done
        echo "*******************************************"
        echo "openstack zone show $zone -c project_id -f value"
        echo "*******************************************"
        zone_project=$(openstack zone show $zone -c project_id -f value)
        echo "*******************************************"
        echo "openstack recordset list $zone -f value --name *amphorae.service.cloud1.example.com."
        echo "*******************************************"
        all_records=$(openstack recordset list $zone -f value --name *amphorae.service.cloud1.example.com. | egrep " A+ " )
        echo "*******************************************"
        echo "openstack port list -c fixed_ips -f value --project $zone_project"
        echo "*******************************************"
        all_project_ips=$(openstack port list -c fixed_ips -f value --project $zone_project)
        declare -A record_info

        for record in $all_records; do
            record_info[ID]=$(echo $record | awk {'print $1'})
            record_info[HOSTNAME]=$(echo $record | awk {'print $3'})
            record_info[TYPE]=$(echo $record | awk {'print $4'})
            record_info[IP]=$(echo $record | awk {'print $5'})
            if ( echo $all_project_ips | grep -qv "${record_info[IP]}[^0-9^a-z]"); then
                echo "${record_info[HOSTNAME]}" >> hostnames.txt
                echo "openstack recordset delete $zone ${record_info[ID]}" >> records.txt
            fi
            for K in "${!record_info[@]}"; do
                unset record_info[$K]
            done
         done
         unset all_project_ips
done