# This script assumes you have {records.txt, clouds.yaml} files already present
# and records.txt is either manually populated OR created using 'clean_dns_records.sh'
# It can be run with following options:
# 
# 1) To delete single recordset:
# 
# python3 delete_rs_argp.py 
#  --cloud <cloud_name_same_as_clouds.yaml> 
#  --rs <uuid> 
#  --zone <uuid>
# 
# 2) To delete bulk recordsets with rs uuid already stored in records.txt
# 
# python3 delete_rs_argp.py 
# --cloud <cloud_name_same_as_clouds.yaml> 
# --zone <uuid>
# --file records.txt 
# --batch <int(batch_size)>

import openstack,yahoo.contrib.keystoneauth1
import argparse as ap
from os import strerror
import logging
import sys
import threading

LOG = logging.getLogger(__name__)


def read_rs_from_file():
    """Reads records from records file"""
    try:
        with open('records.txt', 'r') as f:
            records = f.readlines()
            print(f"Running from read_rs_from_file func:")
            for lines in records:
                print(lines)
    except IOError as e:
        print("I/O Error Occurred : ", strerror(e.errno))


def batch_list(l, batch_size):
    # The number of full list can be made with the input list based on batch size
    batches = int(len(l)/batch_size)

    # Yield the full lists in turn
    for batch in range(batches):
        yield l[batch*batch_size:batch*batch_size+batch_size]

    # If there are any elements leftover yield them too
    if len(l) % batch_size:
        yield l[(batch+1)*batch_size:]


def calculate_threads(filename, batch_size):
    # print(batch_size)
    with open(filename, 'r') as f:
        records = [line.strip() for line in f.readlines()]

        if len(records)%batch_size != 0:
            t_count = round(len(records)/batch_size)+1
        else:
            t_count = round(len(records)/batch_size)   
        # print(f"Thread Count = {t_count}")
    return t_count


def create_thread_lists(filename, batch_size):
    t_list_={}

    #with open('records.txt', 'r') as f:
    with open(filename, 'r') as f:
        records = [line.strip() for line in f.readlines()]
        # batch_size = int(input("Enter batch size : "))

        t_list = list(batch_list(records, batch_size))
        # print(t_list)

        for i in range(len(t_list)):
            t_list_[i] = t_list[i]
            # print(f"Thread_%s : {t_list[i]}" % i)
    
    return t_list_


def exec_thread(*t_list):
    """no.of.threads = no.of.rec / batch_size 
       more no.of.threads = faster execvution + less load on API           
       Runs only on file inputs. Print statements are kept commented for debuggin purpose"""
    # print(type(t_list))
    t_list = list(t_list)
    # print(type(t_list))
    # print(t_list)
    for record_id in range(len(t_list)):
        # for line in t_list_[i]:
        # print(record_id)
        # print(type(record_id))
        record_id = t_list[record_id]
        # print(record_id)
        # print(type(record_id))
        try:
            # st = conn.dns.delete_recordset(i, parser.parse_args().zone)
            conn.dns.delete_recordset(record_id, parser.parse_args().zone)
            print(f"Deleting Recordset: {record_id}")
        except (openstack.exceptions.BadRequestException, openstack.exceptions.ResourceNotFound) as error:
            sys.tracebacklimit = -1
            LOG.exception("ERROR: No Recordset found!!!")
    
    return f"_____Deleted RecordSets: {t_list}"


# def del_rs():
if __name__ == "__main__":

    parser = ap.ArgumentParser()
    parser.add_argument('--cloud', help='(String) Enter Valid CloudName', nargs='?')
    parser.add_argument('--rs', help='(String) RecordSet UUID')
    parser.add_argument('--zone', help='(String) Zone UUID')
    parser.add_argument('--file', help='(String) File Name that contains RecordSets')
    parser.add_argument('--batch', type=int , help='(Integer) Batch Size to split processing in threads')

    parser.parse_args()
    
    conn = openstack.connect(parser.parse_args().cloud)

    batch_size = int(parser.parse_args().batch)

    # print(parser.parse_args().rs)
    if parser.parse_args().rs != None:
        """RS provided manually"""
        st = conn.dns.delete_recordset(parser.parse_args().rs, parser.parse_args().zone)
        print(f"\n\nDeleting:\n\n{st}")

    else:
        """RS provided via file: records.txt"""
        try:
            t_count = calculate_threads('records.txt', batch_size)
            print(f"Thread Count: {t_count}")

            thread_sub_list = create_thread_lists('records.txt', batch_size)
            print(thread_sub_list)

            counter = 0
            while counter < t_count:
                print(f"_____STARTING THREAD: %s ____" % counter)
                # print(thread_sub_list[counter])
                # print(type(thread_sub_list[counter]))
                thread = threading.Thread(target=exec_thread, 
                                          args=(thread_sub_list[counter]))
                thread.start()
                counter += 1
                thread.join()

        except IOError as e:
            print("I/O Error Occurred : ", strerror(e.errno))