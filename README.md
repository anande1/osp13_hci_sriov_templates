![Red Hat Open Stack](https://gitlab.com/redhatsummitlabs/private-cloud-lab-with-openstack-ansible-and-cloudforms/raw/master/images/Logo-RedHat-A-Color-RGB.png)

![version](https://img.shields.io/badge/osp__version-Queens(13.0.4)-brightgreen.svg) ![ceph_version](https://img.shields.io/badge/ceph__version-luminous(3.2)-blueviolet.svg)
# Table of Contents #
  - [Prepare the Undercloud](https://gitlab.com/anande1/osp13_hci_sriov_templates/tree/master#prepare-the-undercloud--openstack-director-system)
  - [Install the undercloud](https://gitlab.com/anande1/osp13_hci_sriov_templates/tree/master#install-the-undercloud)  
  - [Overcloud Image Preparation](https://gitlab.com/anande1/osp13_hci_sriov_templates/tree/master#overcloud-image-preparation)
  - [Creating container registry with Satellite access](https://gitlab.com/anande1/osp13_hci_sriov_templates#creating-container-registry-setup-with--satellite-access)
  - [Creating container registry without satellite (Disconnected)](https://gitlab.com/anande1/osp13_hci_sriov_templates#creating-container-registry-without-satellite-disconnected)
  - [Registering the Baremetal Nodes](https://gitlab.com/anande1/osp13_hci_sriov_templates#registering-the-baremetal-nodes)
  - [Overcloud Preparation](https://gitlab.com/anande1/osp13_hci_sriov_templates#overcloud-preparation)
  


## Prepare the Undercloud / Openstack Director system ##

The following steps are guidelines to deploy Openstack private cloud :cloud: in lab or customer premises.

#### Create the stack user ####
The director installation process requires a non-root user to execute commands. 
Use the following commands to create the user and set a password. 
```
# useradd stack
# passwd stack
```

#### Disable password requirements for the ‘stack’ user when using sudo ####
```
# echo "stack ALL=(root) NOPASSWD:ALL" | tee -a /etc/sudoers.d/stack 
# chmod 0440 /etc/sudoers.d/stack
```

#### Switch to the new ‘stack’ user ####
```
# su - stack
```

#### Create directories for images and templates ####

The director uses system images and Heat templates to create the Overcloud environment. To keep these files organized.
```
# mkdir ~/images
# mkdir ~/custom-templates
```

#### Set hostname as per the value in the OSP-D Hostname FQDN ####
```
# sudo hostnamectl set-hostname <OSPD Hostname>
# sudo hostnamectl set-hostname --transient <OSPD Hostname>
``` 

#### Enter system hostname and base name in /etc/hosts ####

Enter the “OSP-D Hostname” and “OSP-D Hostname FQDN” values, along with the OSP-D’s external IP address in the /etc/hosts file as show below: 
```
# sudo vi /etc/hosts
<External IP for OSPD from CIQ> <OSPD Hostname from PIF> <OSPD Hostname from PIF>. t-mobile.com

[stack@ ospd-vm ~]$ cat /etc/hosts
127.0.0.1     localhost localhost.localdomain localhost4 localhost4.localdomain4
::1           localhost localhost.localdomain localhost6 localhost6.localdomain6
10.84.123.184 rh-satellite.example        rh-satellite.example.com
```

#### Update OSPD Timezone to UTC ####
``` 
# timedatectl
# timedatectl set-timezone UTC
# sudo systemctl
```

#### List the existing repos ####
```
[root@ ospd-vm ~]# yum repolist
Loaded plugins: enabled_repos_upload, package_upload, product-id, search-disabled-repos, subscription-manager
repo id                                                   repo name                                                                           status
!rhel-7-server-extras-rpms/x86_64                         Red Hat Enterprise Linux 7 Server - Extras (RPMs)                                      639
!rhel-7-server-openstack-10-rpms/7.3/x86_64               Red Hat OpenStack Platform 10 for RHEL 7 (RPMs)                                      1,179
!rhel-7-server-rh-common-rpms/7.3/x86_64                  Red Hat Enterprise Linux 7 Server - RH Common (RPMs)                                   228
!rhel-7-server-rpms/7.3/x86_64                            Red Hat Enterprise Linux 7 Server (RPMs)                                            14,619
!rhel-ha-for-rhel-7-server-rpms/7.3/x86_64                Red Hat Enterprise Linux High Availability (for RHEL 7 Server) (RPMs)                  296
repolist: 16,961
Uploading Enabled Reposistories Report
Loaded plugins: product-id
```

*NOTE: Ideally the repo-list should be blank when you are first registering the undercloud to satellite :satellite: , you can see OSP-10 repos here as this system previously was OSP-10 Director*

#### Use the Activation key provided by your Satellite admin to register the undercloud to Satellite ####
```
[root@ ospd-vm ~]# subscription-manager register --org="ACME" --activationkey="rhel76-osp13" --force
The system with UUID a229d0c7-0075-419d-ba75-5cc36e847913 has been unregistered
The system has been registered with ID: 65d523ab-099e-4f8a-8cb9-54cc5870c92b

Installed Product Current Status:
Product Name: Red Hat Enterprise Linux Server
Status:       Subscribed

Product Name: Red Hat OpenStack
Status:       Subscribed
```
*NOTE: Using the --force option will automatically unregister ospd from previous osp-10 repos and register to new osp13 repos*
```
[root@ ospd-vm ~]# yum repolist
Loaded plugins: enabled_repos_upload, package_upload, product-id, search-disabled-repos, subscription-manager
repo id                                                   repo name                                                                           status
!rhel-7-server-extras-rpms/x86_64                         Red Hat Enterprise Linux 7 Server - Extras (RPMs)                                      901
!rhel-7-server-openstack-13-optools-rpms/x86_64           Red Hat OpenStack Platform 13 Operational Tools for RHEL 7 (RPMs)                      118
!rhel-7-server-openstack-13-rpms/x86_64                   Red Hat OpenStack Platform 13 for RHEL 7 (RPMs)                                        827
!rhel-7-server-optional-rpms/x86_64                       Red Hat Enterprise Linux 7 Server - Optional (RPMs)                                 15,296
!rhel-7-server-rh-common-rpms/x86_64                      Red Hat Enterprise Linux 7 Server - RH Common (RPMs)                                   233
!rhel-7-server-rhceph-3-mon-rpms/x86_64                   Red Hat Ceph Storage MON 3 for Red Hat Enterprise Linux 7 Server (RPMs)                157
!rhel-7-server-rhceph-3-tools-rpms/x86_64                 Red Hat Ceph Storage Tools 3 for Red Hat Enterprise Linux 7 Server (RPMs)              219
!rhel-7-server-rpms/x86_64                                Red Hat Enterprise Linux 7 Server (RPMs)                                            20,895
!rhel-ha-for-rhel-7-server-rpms/x86_64                    Red Hat Enterprise Linux High Availability (for RHEL 7 Server) (RPMs)                  498
repolist: 39,144
Uploading Enabled Reposistories Report
Loaded plugins: product-id
```

#### Attach the relevant Openstack Pool to your undercloud system #### 
```
[stack@ ospd-vm ~]$ sudo subscription-manager attach --pool=8ad4fb4c5f9c30220160034442e50b46
Successfully attached a subscription for: Red Hat OpenStack Platform, Standard Support (4 Sockets, NFR, Partner Only)
```
*Note: One needs to find the pool-id before attaching it, this can be done using $sudo subscription-manager list --available  --matches "*Openstack*"*

#### Disable all the repos and enable only the necessary ones ####
```
[stack@ ospd-vm ~]$ sudo subscription-manager repos --disable=*
Repository 'rhel-7-server-rh-common-rpms' is disabled for this system.
Repository 'rhel-7-server-optional-rpms' is disabled for this system.
Repository 'rhel-7-server-rhceph-3-tools-rpms' is disabled for this system.
Repository 'rhel-7-server-rhceph-3-mon-rpms' is disabled for this system.
Repository 'rhel-ha-for-rhel-7-server-rpms' is disabled for this system.
Repository 'rhel-7-server-rpms' is disabled for this system.
Repository 'rhel-7-server-openstack-13-optools-rpms' is disabled for this system.
Repository 'rhel-7-server-extras-rpms' is disabled for this system.
Repository 'rhel-7-server-openstack-13-rpms' is disabled for this system.
```

#### Verify all the repos have been disabled ####
```
[stack@ ospd-vm ~]$ yum repolist
Loaded plugins: enabled_repos_upload, package_upload, product-id, search-disabled-repos, subscription-manager
repolist: 0
Uploading Enabled Reposistories Report
Loaded plugins: product-id
Cannot upload enabled repos report, is this client registered?
```

#### Enable only the necessary repos ####
```
[stack@ ospd-vm ~]$ sudo subscription-manager repos \
--enable=rhel-7-server-rpms \
--enable=rhel-7-server-extras-rpms \
--enable=rhel-7-server-rh-common-rpms \
--enable=rhel-ha-for-rhel-7-server-rpms \
--enable=rhel-7-server-openstack-13-rpms \
--enable=rhel-7-server-rhceph-3-tools-rpms
```

## Install the undercloud ##

#### Customizing undercloud configuration ####

Take care of the below options in the undercloud.conf file for each POD.
````
[stack@ ospd-vm ~]$ awk '$1 ~ /^[^;#]/' undercloud.conf
[DEFAULT]
undercloud_hostname = ospd-vm.example.com
local_ip = 192.200.0.1/8
undercloud_public_host = 192.200.0.2
undercloud_admin_host = 192.200.0.3
local_interface = eth1
masquerade_network = 192.0.0.0/8
undercloud_update_packages = false
clean_nodes = true
[auth]
[ctlplane-subnet]
cidr = 192.0.0.0/8
dhcp_start = 192.200.0.101
dhcp_end = 192.200.0.150
inspection_iprange = 192.200.0.201,192.200.0.250
gateway = 192.200.0.1
````

#### Openstack Undercloud Installation ####

Once the undercloud.conf is edited and saved, run the below command to install additonal packages and configures its services to suit the settings in undercloud.conf. The script takes several minutes to complete.
```
[stack@ ospd-vm ~]$ sudo yum install screen
[stack@ ospd-vm ~]$ screen 

[To exit screen press “cntl+a””d” , to reattach type “screen –r” , to stop press    “cntl+a””k”]

[stack@ ospd-vm ~]$  openstack undercloud install
```
- Go grab a :coffee: while this finishes. 

- Use the following command to install the required command line tools for director installation and configuration:
```
$ sudo yum install -y python-tripleoclient
```
- Use the following to install the ceph-ansible playbooks :
```
$ sudo yum install -y ceph-ansible
```


***NOTE***
  - Was facing issue mentioned in : https://bugzilla.redhat.com/show_bug.cgi?id=1580362
  - Wiped-out the osp10-undercloud as per : https://access.redhat.com/solutions/2210421

#### Verify the Openstack Platform Services #### 

Undercloud installation starts all Openstack Platform Services automatically, check the enabled services using the following command.

```
[stack@ ospd-vm ~]$ . stackrc

(undercloud) [stack@ ospd-vm ~]$ sudo systemctl list-units openstack-*
UNIT                                         LOAD   ACTIVE SUB     DESCRIPTION
openstack-glance-api.service                 loaded active running OpenStack Image Service (code-named Glance) API server
openstack-heat-engine.service                loaded active running Openstack Heat Engine Service
openstack-ironic-conductor.service           loaded active running OpenStack Ironic Conductor service
openstack-ironic-inspector-dnsmasq.service   loaded active running PXE boot dnsmasq service for Ironic Inspector
openstack-ironic-inspector.service           loaded active running Hardware introspection service for OpenStack Ironic
openstack-mistral-api.service                loaded active running Mistral API Server
openstack-mistral-engine.service             loaded active running Mistral Engine Server
openstack-mistral-executor.service           loaded active running Mistral Executor Server
openstack-nova-api.service                   loaded active running OpenStack Nova API Server
openstack-nova-compute.service               loaded active running OpenStack Nova Compute Server
openstack-nova-conductor.service             loaded active running OpenStack Nova Conductor Server
openstack-nova-scheduler.service             loaded active running OpenStack Nova Scheduler Server
openstack-swift-account-reaper.service       loaded active running OpenStack Object Storage (swift) - Account Reaper
openstack-swift-account.service              loaded active running OpenStack Object Storage (swift) - Account Server
openstack-swift-container-sync.service       loaded active running OpenStack Object Storage (swift) - Container Sync
openstack-swift-container-updater.service    loaded active running OpenStack Object Storage (swift) - Container Updater
openstack-swift-container.service            loaded active running OpenStack Object Storage (swift) - Container Server
openstack-swift-object-expirer.service       loaded active running OpenStack Object Storage (swift) - Object Expirer
openstack-swift-object-reconstructor.service loaded active running OpenStack Object Storage (swift) - Object Reconstructor
openstack-swift-object-updater.service       loaded active running OpenStack Object Storage (swift) - Object Updater
openstack-swift-object.service               loaded active running OpenStack Object Storage (swift) - Object Server
openstack-swift-proxy.service                loaded active running OpenStack Object Storage (swift) - Proxy Server
openstack-zaqar@1.service                    loaded active running OpenStack Message Queuing Service (code-named Zaqar) Server Instance 1

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.

23 loaded units listed. Pass --all to see loaded but inactive units, too.
To show all installed unit files use 'systemctl list-unit-files'.
```

#### Verify Ceilometer Services are Not Started ####
```
(undercloud) [stack@ ospd-vm ~]$ sudo systemctl list-unit-files | egrep 'gnocchi|aodh|ceil'
```
- In case Telemetry is missed to set = disable. Please execute below steps to disable undercloud ceilometer/aodh/gnocchi.
```
# sudo su
# systemctl list-unit-files | egrep 'gnocchi|aodh|ceil' | awk '{print $1}' | while read service;do echo $service ; systemctl stop $service ; systemctl disable $service ; done

(undercloud) [stack@tb13-ospd ~]$ sudo systemctl list-unit-files | egrep 'gnocchi|aodh|ceil' | awk '{print $1}' | while read service;do echo $service ; systemctl stop $service ; systemctl disable $service ; done 
```

- Verify if ceilometer services are disabled/not installed in undercloud.
```
# systemctl list-unit-files | egrep 'gnocchi|aodh|ceil'
(undercloud) [stack@tb13-ospd ~]$ sudo systemctl list-unit-files | egrep 'gnocchi|aodh|ceil'
```

## Overcloud Image Preparation ##

#### Installation of disk images for provisioning overcloud nodes ####

This includes:
  1. An introspection kernel and ramdisk - used for bare metal system introspection over PXE boot.
  2. Deployment kernel and ramdisk - used for system provisioning and deployment.
  3. Overcloud kernel, ramdisk, and full image - a base overcloud system that is written to the node’s hard disk. 

Obtain these images from the rhosp-director-images and rhosp-director- images-ipa packages (download 1.6Gb of data): 
```
(undercloud) [stack@ ospd-vm ~]$ exec su -l stack
Password:
Last login: Fri Aug 31 01:49:35 EDT 2018 from 172.25.22.95 on pts/0
[stack@ ospd-vm ~]$ source ~/stackrc

(undercloud) [stack@ ospd-vm ~]$ sudo yum install rhosp-director-images rhosp-director-images-ipa
Loaded plugins: enabled_repos_upload, package_upload, product-id, search-disabled-repos, subscription-manager
rhel-7-server-extras-rpms                                                                                                  | 2.0 kB  00:00:00
rhel-7-server-openstack-13-rpms                                                                                            | 2.3 kB  00:00:00
rhel-7-server-rh-common-rpms                                                                                               | 2.1 kB  00:00:00
rhel-7-server-rpms                                                                                                         | 2.0 kB  00:00:00
rhel-ha-for-rhel-7-server-rpms                                                                                             | 2.0 kB  00:00:00
Package rhosp-director-images-13.0-20180710.2.el7ost.noarch already installed and latest version
Package rhosp-director-images-ipa-13.0-20180710.2.el7ost.noarch already installed and latest version
Nothing to do
Uploading Enabled Reposistories Report
Loaded plugins: product-id, subscription-manager

(undercloud) [stack@ ospd-vm ~]$ cd ~/images/

(undercloud) [stack@ ospd-vm images]$ for i in /usr/share/rhosp-director-images/overcloud-full-latest-13.0.tar /usr/share/rhosp-director-images/ironic-python-agent-latest-13.0.tar; do tar -xvf $i; done
overcloud-full.qcow2
overcloud-full.initrd
overcloud-full.vmlinuz
overcloud-full-rpm.manifest
overcloud-full-signature.manifest
ironic-python-agent.initramfs
ironic-python-agent.kernel
```

#### Customize the Overcloud Image #### 
```
(undercloud) [stack@ ospd-vm images]$ sudo yum install libguestfs-tools virt-manager -y
(undercloud) [stack@ ospd-vm images]$ sudo systemctl start libvirtd 
(undercloud) [stack@ ospd-vm images]$ sudo service libvirtd.service status
edirecting to /bin/systemctl status libvirtd.service
● libvirtd.service - Virtualization daemon
   Loaded: loaded (/usr/lib/systemd/system/libvirtd.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2019-01-22 19:12:52 UTC; 6s ago
     Docs: man:libvirtd(8)
           https://libvirt.org
 Main PID: 25638 (libvirtd)
    Tasks: 17 (limit: 32768)
   CGroup: /system.slice/libvirtd.service
           └─25638 /usr/sbin/libvirtd

Jan 22 19:12:52 ospd-vm.example.com systemd[1]: Starting Virtualization daemon...
Jan 22 19:12:52 ospd-vm.example.com systemd[1]: Started Virtualization daemon.

(undercloud) [stack@ ospd-vm images]$ virt-customize -a overcloud-full.qcow2 --root-password password:1Qa@ws#edTMO --run-command 'sed -i -e "s/.*PasswordAuthentication.*/PasswordAuthentication yes/" /etc/ssh/sshd_config' --run-command 'sed -i -e "s/.*PermitRootLogin.*/PermitRootLogin yes/" /etc/ssh/sshd_config' --run-command 'sed -i -e "s/#UseDNS yes/UseDNS no/" /etc/ssh/sshd_config'
[   0.0] Examining the guest ...
[  18.8] Setting a random seed
[  18.8] Running: sed -i -e "s/.*PasswordAuthentication.*/PasswordAuthentication yes/" /etc/ssh/sshd_config
[  18.8] Running: sed -i -e "s/.*PermitRootLogin.*/PermitRootLogin yes/" /etc/ssh/sshd_config
[  18.8] Running: sed -i -e "s/#UseDNS yes/UseDNS no/" /etc/ssh/sshd_config
[  18.9] Setting passwords
[  21.2] Finishing off
```
***Note:***
  - If the Above fails to run, logout of the OSPD and log back in and try again.
  - Change the above password as per CIQ

#### Stop the libvirtd service ####
```
(undercloud) [stack@ ospd-vm images]$ sudo systemctl stop libvirtd

(undercloud) [stack@ ospd-vm images]$ sudo service libvirtd.service status
Redirecting to /bin/systemctl status libvirtd.service
● libvirtd.service - Virtualization daemon
   Loaded: loaded (/usr/lib/systemd/system/libvirtd.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Tue 2019-01-22 19:16:04 UTC; 42s ago
     Docs: man:libvirtd(8)
           https://libvirt.org
  Process: 25638 ExecStart=/usr/sbin/libvirtd $LIBVIRTD_ARGS (code=exited, status=0/SUCCESS)
 Main PID: 25638 (code=exited, status=0/SUCCESS)

Jan 22 19:12:52 ospd-example.com systemd[1]: Starting Virtualization daemon...
Jan 22 19:12:52 ospd-example.com systemd[1]: Started Virtualization daemon.
```

#### Customize the deployment to disable chronyd and ksm ####
```
$ virt-customize -a overcloud-full.qcow2 --run-command "systemctl disable chronyd"
[   0.0] Examining the guest ...
[   3.0] Setting a random seed
[   3.0] Running: systemctl disable chronyd
[   3.3] Finishing off

$ virt-customize -a overcloud-full.qcow2 --run-command "systemctl disable ksm"
[   0.0] Examining the guest ...
[   4.3] Setting a random seed
[   4.3] Running: systemctl disable ksm
[   5.4] Finishing off

$ virt-customize -a overcloud-full.qcow2 --run-command "systemctl disable ksmtuned"
[   0.0] Examining the guest ...
[   2.7] Setting a random seed
[   2.7] Running: systemctl disable ksmtuned
[   2.9] Finishing off
```

#### Upload these images to Glance Registry ####
```
(undercloud) [stack@ ospd-vm images]$  openstack overcloud image upload --image-path /home/stack/images/
Image "overcloud-full-vmlinuz" was uploaded.
+--------------------------------------+------------------------+-------------+---------+--------+
|                  ID                  |          Name          | Disk Format |   Size  | Status |
+--------------------------------------+------------------------+-------------+---------+--------+
| 9f6a5f42-2fa5-4f99-91ab-bc58290ea7f5 | overcloud-full-vmlinuz |     aki     | 6390064 | active |
+--------------------------------------+------------------------+-------------+---------+--------+
Image "overcloud-full-initrd" was uploaded.
+--------------------------------------+-----------------------+-------------+----------+--------+
|                  ID                  |          Name         | Disk Format |   Size   | Status |
+--------------------------------------+-----------------------+-------------+----------+--------+
| 7477f202-e2f4-4450-8b2e-a7f26aa5c0d4 | overcloud-full-initrd |     ari     | 64133766 | active |
+--------------------------------------+-----------------------+-------------+----------+--------+
Image "overcloud-full" was uploaded.
+--------------------------------------+----------------+-------------+------------+--------+
|                  ID                  |      Name      | Disk Format |    Size    | Status |
+--------------------------------------+----------------+-------------+------------+--------+
| ce4a3639-1e8e-4f81-99a8-d40293ffd398 | overcloud-full |    qcow2    | 1382547456 | active |
+--------------------------------------+----------------+-------------+------------+--------+
Image "bm-deploy-kernel" was uploaded.
+--------------------------------------+------------------+-------------+---------+--------+
|                  ID                  |       Name       | Disk Format |   Size  | Status |
+--------------------------------------+------------------+-------------+---------+--------+
| 7a887247-db1b-411b-9288-7973ffb1e6b8 | bm-deploy-kernel |     aki     | 6390064 | active |
+--------------------------------------+------------------+-------------+---------+--------+
Image "bm-deploy-ramdisk" was uploaded.
+--------------------------------------+-------------------+-------------+-----------+--------+
|                  ID                  |        Name       | Disk Format |    Size   | Status |
+--------------------------------------+-------------------+-------------+-----------+--------+
| 7764f168-2726-4eb0-a7df-a2ab2b887df1 | bm-deploy-ramdisk |     ari     | 425447682 | active |
+--------------------------------------+-------------------+-------------+-----------+--------+

$ openstack image list
 +--------------------------------------+------------------------+--------+
 | ID                                   | Name                   | Status |
 +--------------------------------------+------------------------+--------+
 | e31cff35-aad9-457f-9922-fee1da476b1e | bm-deploy-kernel       | active |
 | 90cdbc30-3e31-45ea-9b60-8c0329d5cd8e | bm-deploy-ramdisk      | active |
 | 02d67a82-9747-4e87-bb8d-c588e0648018 | overcloud-full         | active |
 | 28354b2a-f073-475d-bf55-47588dbbb14e | overcloud-full-initrd  | active |
 | 0af921fd-e51c-4e68-b763-6ad67dd58f3f | overcloud-full-vmlinuz | active |
 +--------------------------------------+------------------------+--------+  
```

***Note: Following sections are not covered in this README***
  * Overcloud CIMC Host Settings
  * Firmware Upgrade

## Creating container registry setup with  Satellite access ##

In OSP-13 - A lot of previous systemd managed services have now been converted to containers that are managed by docker. The images of thse containers are stored Externally on Red Hat Satellite. However, since our lab network topology is such that none of the overcloud nodes can reach the RH Satellite directly, we need to setup the OSPD/Undercloud as the container registry. This means we need to clone the registry on the remote Satellite to local directory on OSPD/Undercloud. 

  - The _Local container images_ are different from the _Local registry images_ on undercloud. 
  - Running docker images only shows the container images in local docker storage, which is completely different from the images stored in the undercloud local registry. Red Hat OpenStack stores all its container images in ```docker-distribution registry``` running on undercloud, different from local docker storage. 
  - Reference: [How to view container images stored on undercloud's local registry](https://access.redhat.com/solutions/3897861)

#### Overcloud Container Image Preparation ####
```
(undercloud) [stack@ ospd-vm ~]$ openstack overcloud container image prepare \
--namespace=rh-satellite.example.com:5000 \
--push-destination=192.200.0.1:8787 \
--prefix=overcloud-osp13_containers- \
--tag-from-label {version}-{release} \
--set ceph_namespace=rh-satellite.example.com:5000 \
--set ceph_image=rhceph-3-rhel7 \
--output-env-file=/tmp/overcloud_images_sat_final.yaml \ 
--output-images-file=/tmp/local_registry_sat_images_final.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
--set ceph_namespace=rh-satellite.example.com:5000 \
--set ceph_image=osp13_containers-rhceph-3-rhel7 \
-e /usr/share/openstack-tripleo-heat-templates/environments/neutron-sriov.yaml \
--roles-file /home/stack/custom-templates/roles_data.yaml 
```

***Note***
* Creating container registry – we hit a few bugs related to : 
  - [Neutron-sriov-agent container missing when using SRIOV environment file](https://bugzilla.redhat.com/show_bug.cgi?id=1581385#c5)
  - [No instructions provided on configuring the sriov-agent container image in RHOSP 13 documentation](https://bugzilla.redhat.com/show_bug.cgi?id=1601313)
  

#### Upload Container Images to Local Resistery ####
```
(undercloud) [stack@ ospd-vm ~]$ openstack overcloud container image upload \
--config-file /home/stack/custom-templates/local_registry_sat_images_final.yaml \
--verbose
```

## Creating container registry without satellite (disconnected) ##

In this scenario, the undercloud/ospd is completely isolated from Satellite. viz. it does not have a direct access to any registry source to sync the images from. So a manual copy/import of the images is required on ospd. Following link explains all the steps related to this. 

[Setup Offline Container Registry](https://gitlab.com/anande1/osp13_hci_sriov_templates/blob/master/offline_docker_registry_setup)

## Registering the Baremetal Nodes ##

This step involves creating a json file that maintains the information about the authentication and IPMI for managing the baremetal nodes. 

#### Create the instackenv.json file
Create a json file in the following format : 
```
{
    "nodes": [
        {
            "capabilities": "node:controller-0,boot_option:local",
            "pm_type": "pxe_ipmitool",
            "pm_user": "username",
            "pm_password": "password",
            "pm_addr": "x.x.x.1"
        },
        {
            "capabilities": "node:osd-compute-0,boot_option:local",
            "pm_type": "pxe_ipmitool",
            "pm_user": "username",
            "pm_password": "password",
            "pm_addr": "x.x.x.3"
        },
.....
(similarly add info for the remaining nodes)
.....
```

#### Verify the Power status of the nodes, make sure all nodes are powered off ####
```
(undercloud) [stack@ ospd-vm ~]$ grep pm_addr instackenv.json | cut -d ":" -f2 | tr -d '"' > ip_list.txt

(undercloud) [stack@ ospd-vm ~]$ while read ip; do ipmitool -I lanplus -H $ip -U username -P password power status ; echo -ne "$ip :"\ ;done < ip_list.txt
```
#### Import baremetal nodes to OSPD ####
```
$ source ~/stackrc
(undercloud) [stack@ ospd-vm ~]$ openstack overcloud node import ~/instackenv.json
```
#### Assign the kernel and ramdisk images to all nodes ####
```
# openstack baremetal configure boot
```
#### Introspect the nodes ####
```
(undercloud) [stack@ ospd-vm ~]$ openstack overcloud node introspect --all-manageable --provide
```
#### Set the boot disks ####
Post Introspection, One can find the disks data using ironic commands mentioned below : 
```
$ openstack baremetal node list | cut -d ' ' -f2 | grep -v ID | grep -v ^+ > uuid_list


(undercloud) [stack@cgvzw214 ~]$ while read uuid; do openstack baremetal node show $uuid | grep node | awk '{print $13}' | cut -d ',' -f1 && echo $uuid && openstack baremetal introspection data save $uuid| jq ".inventory.disks";done < uuid_list 
u'node:compute-0
514ae6aa-0f8d-4bce-8cd1-63881ac6d9d9
[
  {
    "size": 900151926784,
    "serial": "600508b1001c218e84f93b311ba1e169",
    "wwn": "0x600508b1001c218e",
    "rotational": true,
    "vendor": "HP",
    "name": "/dev/sda",
    "wwn_vendor_extension": "0x84f93b311ba1e169",
    "hctl": "0:1:0:0",
    "wwn_with_extension": "0x600508b1001c218e84f93b311ba1e169",
    "by_path": "/dev/disk/by-path/pci-0000:03:00.0-scsi-0:1:0:0",
    "model": "LOGICAL VOLUME"
  }
]
Node cc9f0acc-9b38-42df-bdb7-236e0e64e057 could not be found. (HTTP 404)
cc9f0acc-9b38-42df-bdb7-236e0e64e057
[
  {
    "size": 900151926784,
    "serial": "600508b1001cb40e8a47ed4323d89328",
    "wwn": "0x600508b1001cb40e",
    "rotational": true,
    "vendor": "HP",
    "name": "/dev/sda",
    "wwn_vendor_extension": "0x8a47ed4323d89328",
    "hctl": "0:1:0:0",
    "wwn_with_extension": "0x600508b1001cb40e8a47ed4323d89328",
    "by_path": "/dev/disk/by-path/pci-0000:03:00.0-scsi-0:1:0:0",
    "model": "LOGICAL VOLUME"
  },
  {
    "size": 268435456,
    "serial": "000002660A01",
    "wwn": null,
    "rotational": true,
    "vendor": "HP iLO",
    "name": "/dev/sdb",
    "wwn_vendor_extension": null,
    "hctl": "3:0:0:0",
    "wwn_with_extension": null,
    "by_path": "/dev/disk/by-path/pci-0000:00:1d.0-usb-0:1.3.1:1.0-scsi-0:0:0:0",
    "model": "LUN 00 Media 0"
  }
]
```

#### Tag the nodes with boot-options and root disks ####
This should only be done on nodes having more than 1 disks. 
```
$ openstack baremetal node set --property root_device='{"serial": "600508b1001c218e84f93b311ba1e169"}' 514ae6aa-0f8d-4bce-8cd1-63881ac6d9d9
```

#### Similarly Obtain the NIC card details ####
This step is only for diagnostic purpose and not absolutely necessary. 
```
$ while read uuid; do openstack baremetal node show $uuid | grep node | awk '{print $13}' | cut -d ',' -f1 && echo $uuid && openstack baremetal introspection data save $uuid| jq "." | grep '"name":' |sort | uniq | egrep -v 'sda|ProLian' && openstack baremetal introspection data save $uuid| jq "." | grep mac_addr ;done < uuid_list >> nic_details
```

## Overcloud Preparation ##

This step involves obtaining the templates, modifying them as per current environment requirements and deploying the stack. 

#### Generate composable role for the overcloud #### 
This step is only required if there are new composable roles that are not available in the templates that will be used as part of the deployment - This step is not absolutely necessary. 
```
(undercloud) [stack@ ospd-vm ~]$ openstack overcloud roles generate --roles-path /usr/share/openstack-tripleo-heat-templates/roles -o ~/custom-templates/roles_data.yaml Controller Compute BlockStorage ObjectStorage ComputeHCI

Make sure you add the following 2 lines in the services section in ComputeHCI :
- OS::TripleO::Services::NeutronSriovAgent      # Adding this newline 
- OS::TripleO::Services::NeutronSriovHostConfig # Adding this newline
```

#### Create a dpeloy-script for ease of running the overcloud deployment #### 
```
$ cat deploy-v1.sh 
time openstack overcloud deploy --templates \
-r /home/stack/custom-templates/roles_data.yaml \
-e /home/stack/custom-templates/overcloud_images_sat_final.yaml \
-e /home/stack/custom-templates/scheduler_hints_env.yaml \
-e /home/stack/custom-templates/network-environment.yaml \
-e /home/stack/custom-templates/layout.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/docker.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/docker-ha.yaml \
-e /home/stack/custom-templates/compute.yaml \
-e /home/stack/custom-templates/ceph.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
-e /home/stack/custom-templates/neutron-sriov.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/host-config-and-reboot.yaml \
-t 120 \
--log-file overcloudreDeploy_$(date +%m_%d_%y__%H_%M_%S).log \
--libvirt-type kvm
```

Run the overcloud script (make sure you have sourced stackrc file) 
```
(undercloud) [stack@ ospd-vm ~]$ chmod +x custom-templates/deploy-v1.sh 
(undercloud) [stack@ ospd-vm ~]$ ./custom-templates/deploy-v1.sh 
```
This will take a lot of depending on your number of nodes. The hard timeout is 120mins.

Once the stack is deployed, you should see a status similar to following :
```
(undercloud) [stack@ ospd-vm ~]$ openstack stack list 
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
| ID                                   | Stack Name | Project                          | Stack Status    | Creation Time        | Updated Time         |
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
| 5306a5ca-7a9c-469b-9723-28b48069fe16 | overcloud  | 4d8e099eefa54f5ebd3d77cb6a34d367 | CREATE_COMPLETE | 2019-03-28T16:40:51Z | 2019-03-30T02:43:42Z |
+--------------------------------------+------------+----------------------------------+-----------------+----------------------+----------------------+
```

#### NOVA-Cells: Graphviz Diagram Test:

![Alt text](https://g.gravizo.com/svg?%20%20digraph%20myservices%20{%20%20%20%20graph%20[pad=%220.35%22,%20ranksep=%220.65%22,%20nodesep=%220.55%22,%20concentrate=true];%20%20%20%20node%20[fontsize=10%20fontname=%22Monospace%22];%20%20%20%20edge%20[arrowhead=%22normal%22,%20arrowsize=%220.8%22];%20%20%20%20labelloc=bottom;%20%20%20%20labeljust=left;%20%20%20%20subgraph%20api%20{%20%20%20%20%20%20api%20[label=%22nova-api%22]%20%20%20%20%20%20scheduler%20[label=%22nova-scheduler%22]%20%20%20%20%20%20conductor%20[label=%22super%20conductor%22]%20%20%20%20%20%20{%20rank=same%20%20%20%20%20%20%20%20apimq%20[label=%22API%20MQ%22%20shape=%22diamond%22]%20%20%20%20%20%20%20%20apidb%20[label=%22API%20Database%22%20shape=%22box%22]%20%20%20%20%20%20}%20%20%20%20%20%20api%20-%3E%20apimq%20-%3E%20conductor%20%20%20%20%20%20api%20-%3E%20apidb%20%20%20%20%20%20conductor%20-%3E%20apimq%20-%3E%20scheduler%20%20%20%20%20%20conductor%20-%3E%20apidb%20%20%20%20}%20%20%20%20subgraph%20clustercell0%20{%20%20%20%20%20%20label=%22Cell:0%22%20%20%20%20%20%20color=green%20%20%20%20%20%20cell0db%20[label=%22Cell%20Database%22%20shape=%22box%22]%20%20%20%20}%20%20%20%20subgraph%20clustercell1%20{%20%20%20%20%20%20label=%22Cell:1%22%20%20%20%20%20%20color=blue%20%20%20%20%20%20mq1%20[label=%22Cell%20MQ%22%20shape=%22diamond%22]%20%20%20%20%20%20cell1db%20[label=%22Cell%20Database%22%20shape=%22box%22]%20%20%20%20%20%20conductor1%20[label=%22nova-conductor%22]%20%20%20%20%20%20compute1%20[label=%22nova-compute%22]%20%20%20%20%20%20conductor1%20-%3E%20mq1%20-%3E%20compute1%20%20%20%20%20%20conductor1%20-%3E%20cell1db%20%20%20%20}%20%20%20%20subgraph%20clustercell2%20{%20%20%20%20%20%20label=%22Cell:2%22%20%20%20%20%20%20color=red%20%20%20%20%20%20mq2%20[label=%22Cell%20MQ%22%20shape=%22diamond%22]%20%20%20%20%20%20cell2db%20[label=%22Cell%20Database%22%20shape=%22box%22]%20%20%20%20%20%20conductor2%20[label=%22nova-conductor%22]%20%20%20%20%20%20compute2%20[label=%22nova-compute%22]%20%20%20%20%20%20conductor2%20-%3E%20mq2%20-%3E%20compute2%20%20%20%20%20%20conductor2%20-%3E%20cell2db%20%20%20%20}%20%20%20%20api%20-%3E%20mq1%20-%3E%20conductor1%20%20%20%20api%20-%3E%20mq2%20-%3E%20conductor2%20%20%20%20api%20-%3E%20cell0db%20%20%20%20api%20-%3E%20cell1db%20%20%20%20api%20-%3E%20cell2db%20%20%20%20conductor%20-%3E%20cell0db%20%20%20%20conductor%20-%3E%20cell1db%20%20%20%20conductor%20-%3E%20mq1%20%20%20%20conductor%20-%3E%20cell2db%20%20%20%20conductor%20-%3E%20mq2%20%20})
<details> 
<summary></summary>
custom_mark10
  digraph myservices {
    graph [pad="0.35", ranksep="0.65", nodesep="0.55", concentrate=true];
    node [fontsize=10 fontname="Monospace"];
    edge [arrowhead="normal", arrowsize="0.8"];
    labelloc=bottom;
    labeljust=left;

    subgraph api {
      api [label="nova-api"]
      scheduler [label="nova-scheduler"]
      conductor [label="super conductor"]
      { rank=same
        apimq [label="API MQ" shape="diamond"]
        apidb [label="API Database" shape="box"]
      }

      api -> apimq -> conductor
      api -> apidb
      conductor -> apimq -> scheduler
      conductor -> apidb
    }

    subgraph clustercell0 {
      label="Cell:0"
      color=green
      cell0db [label="Cell Database" shape="box"]
    }

    subgraph clustercell1 {
      label="Cell:1"
      color=blue
      mq1 [label="Cell MQ" shape="diamond"]
      cell1db [label="Cell Database" shape="box"]
      conductor1 [label="nova-conductor"]
      compute1 [label="nova-compute"]

      conductor1 -> mq1 -> compute1
      conductor1 -> cell1db

    }

    subgraph clustercell2 {
      label="Cell:2"
      color=red
      mq2 [label="Cell MQ" shape="diamond"]
      cell2db [label="Cell Database" shape="box"]
      conductor2 [label="nova-conductor"]
      compute2 [label="nova-compute"]

      conductor2 -> mq2 -> compute2
      conductor2 -> cell2db
    }

    api -> mq1 -> conductor1
    api -> mq2 -> conductor2
    api -> cell0db
    api -> cell1db
    api -> cell2db

    conductor -> cell0db
    conductor -> cell1db
    conductor -> mq1
    conductor -> cell2db
    conductor -> mq2
  }
custom_mark10
</details>

#### Openstack OCTAVIA Loadbalancer state-change diagram:

![Alt Text](https://g.gravizo.com/svg?digraph%20lb_state_change%20{%20%20graph%20[pad=%220.35%22,%20ranksep=%220.65%22,%20nodesep=%220.55%22,%20concentrate=true];%20%20node%20[fontsize=10%20fontname=%22Monospace%22];%20%20edge%20[arrowhead=%22normal%22,%20arrowsize=%220.6%22];%20%20%20%20subgraph%20lb_states{%20%20%20%20%20%20node%20[shape=%22ellipse%22%20color=%22blue%22];%20%20%20%20%20%20LB_CREATE,%20LB_UPDATE,%20LB_DELETE%20[shape=%22diamond%22%20color=%22black%22];%20%20%20%20%20%20ACTIVE,%20DELETED%20[shape=%22box%22%20color=%22green%22];%20%20%20%20%20%20ERROR%20[style=%22radial%22%20fillcolor=%22red%22]%20%20%20%20%20%20LB_CREATE%20-%3E%20PENDING_CREATE%20%20-%3E%20ACTIVE;%20%20%20%20%20%20LB_UPDATE%20-%3E%20PENDING_UPDATE%20-%3E%20ERROR;%20%20%20%20%20%20PENDING_CREATE%20%20-%3E%20ERROR;%20%20%20%20%20%20LB_DELETE%20-%3E%20PENDING_DELETE%20-%3E%20ERROR;%20%20%20%20%20%20PENDING_DELETE%20-%3E%20DELETED;%20%20%20%20%20%20PENDING_UPDATE%20-%3E%20ACTIVE;%20%20}})
<details> 
<summary></summary>
<img src='https://g.gravizo.com/svg?
digraph lb_state_change {
  graph [pad="0.35", ranksep="0.65", nodesep="0.55", concentrate=true];
  node [fontsize=10 fontname="Monospace"];
  edge [arrowhead="normal", arrowsize="0.6"];
  
  subgraph lb_states{
      node [shape="ellipse" color="blue"];
      LB_CREATE, LB_UPDATE, LB_DELETE [shape="diamond" color="black"];
      ACTIVE, DELETED [shape="box" color="green"];
      ERROR [style="radial" fillcolor="red"]
      LB_CREATE -> PENDING_CREATE  -> ACTIVE;
      LB_UPDATE -> PENDING_UPDATE -> ERROR;
      PENDING_CREATE  -> ERROR;
      LB_DELETE -> PENDING_DELETE -> ERROR;
      PENDING_DELETE -> DELETED;
      PENDING_UPDATE -> ACTIVE;
  }
}
'>
</details>