#https://bugzilla.redhat.com/show_bug.cgi?id=1506700
#!/bin/bash

_VLAN="305"
_SLEEP="60"
_THREADS=4

# Define net network
_NET="172.16"

while :;
do
        for _NUM in $(seq 0 ${_THREADS});
        do
                # Interface Name
                _IFACE="vlan${_VLAN}_${_NUM}"

                # Generate every time a random MAC
                ovs-vsctl --may-exist add-port br-provider ${_IFACE} tag=${_VLAN} -- set interface ${_IFACE} type=internal

                # Disable IPv6 Link local
                echo 1 > /proc/sys/net/ipv6/conf/${_IFACE}/disable_ipv6

                # Bring it up
                ip link set up ${_IFACE}

                # Add the new network to the local interface
                _3RD="$(( RANDOM % 254 ))"

                ip addr add ${_NET}.${_3RD}.1/16 dev ${_IFACE}

                # Random UNICAST traffic
                timeout --kill-after=10s 10s ping -f ${_NET}.${_3RD}.$(( ( RANDOM % 254 ) + 2 )) &
        done

        # Wait for all of the above threads to end
        wait

        for _NUM in $(seq 0 ${_THREADS});
        do
                # Interface Name
                _IFACE="vlan${_VLAN}_${_NUM}"

                # Shut down old interface
                ip link set down ${_IFACE}

                # Delete old interface
                ovs-vsctl --if-exists del-port br-provider ${_IFACE}
        done

        # Sleep for sometime
        sleep ${_SLEEP}s
done