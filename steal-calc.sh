# Read /proc/stat file (for first datapoint)
read cpu user nice system idle iowait irq softirq steal guest< /proc/stat


# compute active and total utilizations
cpu_steal_prev=$((steal))
cpu_total_prev=$((user+system+nice+softirq+steal+idle+iowait))


sleep 0.05


# Read /proc/stat file (for second datapoint)
read cpu user nice system idle iowait irq softirq steal guest< /proc/stat


# compute active and total utilizations
cpu_steal_cur=$((steal))
cpu_total_cur=$((user+system+nice+softirq+steal+idle+iowait))


# compute CPU steal (%)
if [ $(( cpu_steal_cur-cpu_steal_prev )) -eq 0 ]; then
steal_pct=0
else
steal_pct=$((100*( cpu_steal_cur-cpu_steal_prev ) / (cpu_total_cur-cpu_total_prev) ))
fi


#printf " Current CPU Steal : %s\n" "$steal_pct"


if [[ $steal_pct -gt 50 ]]; then
        echo "CRITICAL - Steal "$steal_pct"%"
        exit 2
fi


if [[ $steal_pct -gt 20 ]]; then
        echo "WARNING - Steal "$steal_pct"%"
        exit 1
fi


if [[ $steal_pct == 0 ]]; then
        echo "OK - Steal "$steal_pct"%"
        exit 0
fi


if [[ $steal_pct -lt 5 ]]; then
        echo "OK - Steal "$steal_pct"%"
        exit 0
fi


exit 0
