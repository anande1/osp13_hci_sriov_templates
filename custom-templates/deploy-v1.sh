## Note: The default templates included below with absolute paths
## starting with /usr/share/.. are not actually included in the
## custom-templates directory. These templates are available on 
## ospd itself, One does not need to modify these. Just use the 
## path of these templates as it is. 


time openstack overcloud deploy --templates \
-r /home/stack/custom-templates/roles_data.yaml \
-e /home/stack/custom-templates/overcloud_images_sat_final.yaml \
-e /home/stack/custom-templates/scheduler_hints_env.yaml \
-e /home/stack/custom-templates/network-environment.yaml \
-e /home/stack/custom-templates/layout.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/docker.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/docker-ha.yaml \
-e /home/stack/custom-templates/compute.yaml \
-e /home/stack/custom-templates/ceph.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/ceph-ansible/ceph-ansible.yaml \
-e /home/stack/custom-templates/neutron-sriov.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/host-config-and-reboot.yaml \
-t 120 \
--log-file overcloudreDeploy_$(date +%m_%d_%y__%H_%M_%S).log \
--libvirt-type kvm 
#--validation-warnings-fatal \
#--log-file overcloudDeploy_$(date +%m_%d_%y__%H_%M_%S).log \
#--debug \
#--verbose
