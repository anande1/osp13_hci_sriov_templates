time openstack overcloud deploy --templates \
--update-plan-only \
-r /home/stack/custom-templates/custom_roles_osp13.yaml \
-e /home/stack/custom-templates/overcloud_images_sat.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/puppet-pacemaker.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/network-isolation.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/storage-environment.yaml \
-e /usr/share/openstack-tripleo-heat-templates/environments/neutron-sriov.yaml \
-e /home/stack/custom-templates/network.yaml \
-e /home/stack/custom-templates/network_data.yaml \
-e /home/stack/custom-templates/ceph.yaml \
-e /home/stack/custom-templates/compute.yaml \
-e /home/stack/custom-templates/layout.yaml \
--libvirt-type kvm \
--control-scale 3 \
--compute-scale 1 \
--control-flavor baremetal \
--compute-flavor baremetal \
#--validation-warnings-fatal \
#--log-file overcloudDeploy_$(date +%m_%d_%y__%H_%M_%S).log \
#--ntp-server 10.81.254.202 \
--timeout 90
